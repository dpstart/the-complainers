// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

$(window).scroll(function() {
    if ($(window).scrollTop() !== 0) {
        $('.title-area').addClass('cust-sticked');
		$('.top-bar').addClass('cust-sticked');
    }
    else {
        $('.title-area').removeClass('cust-sticked');
		$('.top-bar').removeClass('cust-sticked');
    }
});